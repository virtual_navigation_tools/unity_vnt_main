using System.Collections.Generic;
using UnityEngine;

namespace unibi.vnt.levelgen
{
    public class ExampleWorldGen : MonoBehaviour
    {
        #region Fields

        [SerializeField]
        private DistanceBasedTileManager distanceBasedTileManager = null;

        [SerializeField]
        private int floorAreaRadius = 25;

        [SerializeField]
        private Vector3 goalPosition = new();

        [SerializeField]
        private Vector3 homingStartPosition = new();

        private bool isRunning = false;

        [SerializeField]
        private List<Vector3> landmarkPositions = new();

        [SerializeField]
        private GameObject playerObject = null;

        [SerializeField, Range(1, 100)]
        private float resetIntervalInSeconds = 10;

        [SerializeField]
        private float sceneObjectDensity = 0f;

        [SerializeField]
        private SceneSetup sceneSetup = null;

        [SerializeField]
        private bool simulateNewTrial = false;

        private float timeSinceReset = 0;

        [SerializeField]
        private bool updateTiles = false;

        [SerializeField]
        private List<Vector3> waypointPositions = new();

        #endregion Fields

        #region Private Methods

        private void Awake()
        {
            // set target fps to 60
            Application.targetFrameRate = 60;
        }

        private void Start()
        {
            // we create a scene with given parameters
            sceneSetup.SetSceneForTrial
                (
                    landmarkPositions,
                    goalPosition,
                    homingStartPosition,
                    waypointPositions,
                    sceneObjectDensity
                );
        }

        private void Update()
        {
            if (simulateNewTrial)
            {
                timeSinceReset += Time.deltaTime;
                if (timeSinceReset >= resetIntervalInSeconds)
                {
                    sceneSetup.SetSceneForTrial
                        (
                            landmarkPositions,
                            goalPosition,
                            homingStartPosition,
                            waypointPositions,
                            sceneObjectDensity
                        );
                    timeSinceReset = 0;
                    Debug.Log("starting new trial");
                }
            }

            // during the game, we can toggle whether the tiles should be updated around the player
            if (updateTiles == true && isRunning == false)
            {
                distanceBasedTileManager.StartDistanceBasedTileHandling(playerObject, floorAreaRadius);
                isRunning = true;
            }
            else if (updateTiles == false && isRunning == true)
            {
                distanceBasedTileManager.StopDistanceBasedTileHandling();
                isRunning = false;
            }
        }

        #endregion Private Methods
    }
}