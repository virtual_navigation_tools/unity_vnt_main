﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace unibi.vnt.levelgen
{
    public class SceneSetup : MonoBehaviour
    {
        #region Fields

        public static EventHandler SetupDone;

        [SerializeField]
        private GameObject goalMarker = null;

        [SerializeField]
        private ObjectSpawner objectSpawner = null;

        #endregion Fields

        #region Public Methods

        /// <summary>
        /// Removes all landmark objects from the attached ObjectSpawner.
        /// </summary>
        public void RemoveLandmarks()
        {
            objectSpawner.ClearLandmarks();
        }

        /// <summary>
        /// Toggles visibility of the attached goalMarker.
        /// </summary>
        /// <param name="state"> </param>
        public void SetGoalVisible(bool state)
        {
            goalMarker.gameObject.SetActive(state);
        }

        /// <summary>
        /// Creates a new environment with given landmarks, a goal marker and the given density of vegetation.
        /// </summary>
        /// <param name="landmarkPositions"> where landmarks will be placed during the trial </param>
        /// <param name="goalPosition"> where the trial will start and end </param>
        /// <param name="homingStartPosition"> where homing will begin </param>
        /// <param name="waypointPositions"> where waypoints will be placed </param>
        /// <param name="foliagePerSqM"> how much vegetation to place on the ground </param>
        /// <param name="bufferSizeinMeters">
        /// how far to extend the floor around the relevant locations
        /// </param>
        public void SetSceneForTrial
            (
                List<Vector3> landmarkPositions,
                Vector3 goalPosition,
                Vector3 homingStartPosition,
                List<Vector3> waypointPositions,
                float foliagePerSqM,
                float bufferSizeinMeters = 100
            )
        {
            objectSpawner.ResetForNextTrial();
            SetFloorForTrial
                (
                    goalPosition,
                    homingStartPosition,
                    landmarkPositions,
                    waypointPositions,
                    foliagePerSqM,
                    bufferSizeinMeters
                );
            ShowLandmarksAt(landmarkPositions);
            SetGoalForTrial(goalPosition);
        }

        /// <summary>
        /// Resets landmarks for current trial and shows landmarks at new given locations. (Useful
        /// for cue conflict trials where landmarks are displaced during the trial)
        /// </summary>
        /// <param name="positions"> where to show the new landmarks </param>
        public void ShowLandmarksAt(List<Vector3> positions)
        {
            objectSpawner.ClearLandmarks();
            if (positions.Count > 0)
            {
                objectSpawner.PlaceLandmarksAt(positions);
            }
        }

        #endregion Public Methods

        #region Private Methods

        private Rect GetFloorDimensions(List<Vector3> relevantPositions, float bufferSize)
        {
            if (bufferSize < 0)
            {
                throw new ArgumentOutOfRangeException("Buffer must not be negative!");
            }
            float xMin = 0;
            float xMax = 0;
            float zMin = 0;
            float zMax = 0;
            foreach (var item in relevantPositions)
            {
                if (item.x < xMin)
                {
                    xMin = item.x;
                }
                if (item.x > xMax)
                {
                    xMax = item.x;
                }
                if (item.z < zMin)
                {
                    zMin = item.z;
                }
                if (item.z > zMax)
                {
                    zMax = item.z;
                }
            }
            xMin -= bufferSize;
            xMax += bufferSize;
            zMin -= bufferSize;
            zMax += bufferSize;

            return new Rect(xMin, xMax, zMin, zMax);
        }

        private void OnDestroy()
        {
            ObjectSpawner.FloorTilesPlaced -= OnFloorPlaced;
        }

        private void OnFloorPlaced(object sender, EventArgs e)
        {
            SetupDone?.Invoke(this, EventArgs.Empty);
        }

        private void SetFloorForTrial
            (
                Vector3 goalPosition,
                Vector3 homingStartPosition,
                List<Vector3> landmarkPositions,
                List<Vector3> waypointPositions,
                float foliagePerSqM,
                float bufferSize = 100
            )
        {
            // get needed area from trialSettings
            List<Vector3> relevantPositions = new List<Vector3>()
        {
            goalPosition,
            homingStartPosition
        };
            relevantPositions.AddRange(landmarkPositions);
            relevantPositions.AddRange(waypointPositions);

            // create tiled floor around objects and waypoints
            Rect floorDims = GetFloorDimensions(relevantPositions, bufferSize);
            objectSpawner.CreateTiledFloor(floorDims, foliagePerSqM);
        }

        private void SetGoalForTrial(Vector3 goalPosition)
        {
            goalMarker.transform.position = goalPosition;
        }

        private void Start()
        {
            ObjectSpawner.FloorTilesPlaced += OnFloorPlaced;
        }

        #endregion Private Methods
    }
}