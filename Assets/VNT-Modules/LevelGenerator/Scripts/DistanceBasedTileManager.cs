﻿using System;
using System.Collections;
using System.Collections.Generic;
using unibi.vnt.util;
using UnityEngine;

namespace unibi.vnt.levelgen
{
    public class DistanceBasedTileManager : TileManager, ILoggable
    {
        #region Fields

        private float bufferSizeInMeters;
        private float edgeLengthHorz;
        private float edgeLengthVert;

        [SerializeField]
        private ObjectSpawner objectSpawner = null;

        private Coroutine tileUpdateRoutine = null;

        [SerializeField]
        private float tileUpdatesPerSecond = 1;

        private bool updateinProgress = false;
        private bool updateTiles = false;

        [SerializeField]
        private VisibilityController visibilityController = null;

        #endregion Fields

        #region Properties

        public ICustomLogger Logger { get; set; }

        #endregion Properties

        #region Public Methods

        /// <summary>
        /// Starts the process of updating the visible floor. The actual updating happens in a
        /// couroutine and the interval of the updates can be set in the inpsector.
        /// </summary>
        /// <param name="playerObject"> the object on which the floor will be centred </param>
        /// <param name="radius">
        /// the radius in units around the central object to be covered by floor tiles
        /// </param>
        public void StartDistanceBasedTileHandling(GameObject playerObject, int radius)
        {
            updateTiles = true;
            tileUpdateRoutine = StartCoroutine(TileUpdateCoroutine(playerObject, radius));
        }

        /// <summary>
        /// Stops updating the visible floor around the player.
        /// </summary>
        public void StopDistanceBasedTileHandling()
        {
            if (updateTiles == true)
            {
                updateTiles = false;
                StopCoroutine(tileUpdateRoutine);
            }
        }

        /// <summary>
        /// Updates the scene with floortiles around the player in a given radius.
        /// </summary>
        /// <param name="playerObject"> the object on which the floor will be centred </param>
        /// <param name="radiusInUnits">
        /// the radius in units around the central object to be covered by floor tiles
        /// </param>
        public void UpdateTiles(GameObject playerObject, int radiusInUnits)
        {
            updateinProgress = true;
            bufferSizeInMeters = radiusInUnits;
            Vector3 playerPos = playerObject.transform.position;

            var currentTilePositions = GetTilePositionsFromDictionary();
            var requiredPositions = GetRequiredTilePositions(playerPos);

            // find out which tiles can be removed
            var toRemove = new HashSet<int>();
            foreach (var tile in Tiles)
            {
                var canBeRemoved = true;
                // loop through required positions
                foreach (var pos in requiredPositions)
                {
                    // unity fuzzy equality check for positions
                    if (pos == tile.Value.Dimensions.Center)
                    {
                        canBeRemoved = false;
                        break;
                    }
                }
                if (canBeRemoved)
                {
                    toRemove.Add(tile.Key);
                }
            }
            // remove unused tiles
            foreach (var key in toRemove)
            {
                visibilityController.RemoveObjectsFromTracking(Tiles[key].TileObject);
                DestroyTile(key, destroyAttachedObject: true);
            }

            // find out which tiles need to be newly created
            foreach (var requiredPos in requiredPositions)
            {
                var isNeeded = true;
                // check if one of the current tiles is at the right place
                foreach (var currentPos in currentTilePositions)
                {
                    // unity fuzzy equality check for positions
                    if (currentPos == requiredPos)
                    {
                        isNeeded = false;
                        break;
                    }
                }
                // create new tile at given position
                if (isNeeded)
                {
                    float xMin = requiredPos.x - edgeLengthHorz / 2;
                    float xMax = xMin + edgeLengthHorz;
                    float zMin = requiredPos.z - edgeLengthVert / 2;
                    float zMax = zMin + edgeLengthVert;
                    _ = objectSpawner.CreateFloorTile(xMin, xMax, zMin, zMax);
                }
            }
            updateinProgress = false;
        }

        #endregion Public Methods

        #region Private Methods

        private HashSet<Vector3> GetRequiredTilePositions(Vector3 playerPosition)
        {
            FloorTile closestTile = GetTileClosestToPlayer(playerPosition);

            // redo this: calculate number of tile steps from buffer size loop goes number of steps
            var xSteps = Math.Ceiling(bufferSizeInMeters / edgeLengthHorz);
            var ZSteps = Math.Ceiling(bufferSizeInMeters / edgeLengthVert);

            var xMin = closestTile.Dimensions.Center.x - edgeLengthHorz * xSteps;
            var xMax = closestTile.Dimensions.Center.x + edgeLengthHorz * xSteps;
            var zMin = closestTile.Dimensions.Center.z - edgeLengthVert * ZSteps;
            var zMax = closestTile.Dimensions.Center.z + edgeLengthVert * ZSteps;

            var requiredTilesPositions = new HashSet<Vector3>();

            for (var x = xMin; x <= xMax; x += edgeLengthHorz)
            {
                for (var z = zMin; z <= zMax; z += edgeLengthVert)
                {
                    var newPos = new Vector3((float)x, closestTile.Dimensions.Center.y, (float)z);
                    requiredTilesPositions.Add(newPos);
                }
            }

            return requiredTilesPositions;
        }

        private FloorTile GetTileClosestToPlayer(Vector3 playerPosition)
        {
            float minDistSq = float.MaxValue;
            FloorTile closestTile = null;

            foreach (var tile in Tiles)
            {
                float currDistSq = FastDistanceComparison.SquareDistance(tile.Value.Dimensions.Center, playerPosition);
                if (currDistSq < minDistSq)
                {
                    minDistSq = currDistSq;
                    closestTile = tile.Value;
                }
            }

            // rough approximation, this could still lead to player off the tile for non-sqaure
            // tiles (literal corner case)
            float longerSideLength = Math.Max(edgeLengthHorz, edgeLengthVert);
            if (!FastDistanceComparison.IsWithinRadius(closestTile.Dimensions.Center, playerPosition, longerSideLength))
            {
                Logger.Log($"Error: now tile found close to player at {playerPosition}." +
                    $" This should never happen!", LogType.Error, this);
                return null;
            }
            else
            {
                return closestTile;
            }
        }

        private HashSet<Vector3> GetTilePositionsFromDictionary()
        {
            var tilePositions = new HashSet<Vector3>();
            foreach (var tile in Tiles)
            {
                tilePositions.Add(tile.Value.Dimensions.Center);
            }

            return tilePositions;
        }

        private void OnDestroy()
        {
            StopDistanceBasedTileHandling();
        }

        private void Start()
        {
            Debug.Log($"received tile dims: {objectSpawner.TileBounds.x}/{objectSpawner.TileBounds.z}");
            edgeLengthHorz = objectSpawner.TileBounds.x;
            edgeLengthVert = objectSpawner.TileBounds.z;
        }

        private IEnumerator TileUpdateCoroutine(GameObject playerObject, int radius)
        {
            WaitForSecondsRealtime wait = new WaitForSecondsRealtime(1 / tileUpdatesPerSecond);
            while (updateTiles)
            {
                yield return wait;
                if (!updateinProgress)
                {
                    Logger.Log($"performing tile update at {Time.time}", LogType.Log, this);
                    UpdateTiles(playerObject, radius);
                }
            }
        }

        #endregion Private Methods
    }
}