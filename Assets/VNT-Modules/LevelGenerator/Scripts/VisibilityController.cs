﻿using System.Collections.Generic;
using unibi.vnt.util;
using UnityEngine;

namespace unibi.vnt.levelgen
{
    public class VisibilityController : MonoBehaviour
    {
        #region Fields

        [SerializeField]
        private bool isActive = false;

        [SerializeField]
        private GameObject player = null;

        [SerializeField]
        private float renderDistance = 100;

        [SerializeField]
        private HashSet<GameObject> trackedObjects = new HashSet<GameObject>();

        #endregion Fields

        #region Public Methods

        /// <summary>
        /// Add objects to be tracked by the VisibilityController.
        /// </summary>
        /// <param name="newObjects"> the new objects to track </param>
        public void AddObjectsToTrack(List<GameObject> newObjects)
        {
            foreach (var item in newObjects)
            {
                trackedObjects.Add(item);
            }
        }

        /// <summary>
        /// Add an object to be tracked by the VisibilityController.
        /// </summary>
        /// <param name="newObject"> the new object to track </param>
        public void AddObjectsToTrack(GameObject newObject)
        {
            trackedObjects.Add(newObject);
        }

        /// <summary>
        /// Remove objects from tracking.
        /// </summary>
        /// <param name="objectsToRemove"> the objects to remove </param>
        public void RemoveObjectsFromTracking(List<GameObject> objectsToRemove)
        {
            foreach (var item in objectsToRemove)
            {
                trackedObjects.Remove(item);
            }
        }

        /// <summary>
        /// Remove an object from tracking.
        /// </summary>
        /// <param name="objectToRemove"> the object to remove </param>
        public void RemoveObjectsFromTracking(GameObject objectToRemove)
        {
            trackedObjects.Remove(objectToRemove);
        }

        #endregion Public Methods

        #region Private Methods

        private void LateUpdate()
        {
            if (isActive)
            {
                ShowObjectsBasedOnRadius();
            }
        }

        private void ShowObjectsBasedOnRadius()
        {
            foreach (var obj in trackedObjects)
            {
                if (obj == null)
                {
                    // hotfix for desync between tile manager and this
                    return;
                }
                if (FastDistanceComparison.IsWithinRadius(player.transform.position, obj.transform.position, renderDistance))
                {
                    obj.SetActive(true);
                }
                else
                {
                    obj.SetActive(false);
                }
            }
        }

        #endregion Private Methods
    }
}