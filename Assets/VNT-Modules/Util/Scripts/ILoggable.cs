﻿namespace unibi.vnt.util
{
    public interface ILoggable
    {
        #region Properties

        /// <summary>
        /// The attached logger of this Loggable
        /// </summary>
        ICustomLogger Logger { get; set; }

        #endregion Properties
    }
}