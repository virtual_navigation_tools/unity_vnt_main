﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace unibi.vnt.util
{
    public static class FileTools
    {
        #region Public Methods

        /// <summary>
        /// Fetches files from target directory, with given options.
        /// </summary>
        /// <param name="path"> where to look for the files </param>
        /// <param name="pattern"> naming pattern </param>
        /// <param name="exclude"> exclusion list </param>
        /// <param name="searchOption"> IO search option </param>
        /// <returns> </returns>
        public static IEnumerable<string> GetFiles
            (
                string path,
                string pattern = "*.*",
                string[] exclude = null,
                SearchOption searchOption = SearchOption.AllDirectories
            )
        {
            var files = Directory.EnumerateFiles(path, pattern, searchOption);
            if (exclude != null && exclude.Length > 0)
            {
                files = files.Where(f => !IsExcluded(f, exclude));
            }
            return files;
        }

        /// <summary>
        /// Checks whether a string matches one of the patterns in the exlusion list.
        /// </summary>
        /// <param name="fileName"> the string to check </param>
        /// <param name="toExclude"> the list of things to exclude </param>
        /// <returns> </returns>
        public static bool IsExcluded(string fileName, string[] toExclude)
        {
            bool result = false;
            foreach (var item in toExclude)
            {
                if (fileName.Contains(item))
                {
                    return true;
                }
            }
            return result;
        }

        #endregion Public Methods
    }
}