﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;

namespace unibi.vnt.util
{
    public class FileLogger : MonoBehaviour, ICustomLogger, ILoggable
    {
        #region Fields

        private string currentLogPath = "";
        private bool hasInitilaised = false;

        [SerializeField, Tooltip("header line")]
        private string header = "";

        [SerializeField, Tooltip("should the logger record right now?")]
        private bool isActive = false;

        private string logDir = "";

        [SerializeField, Tooltip("name of the log to be created (with appended timestamp created when logger gets enabled)")]
        private string logFileName = "new_log";

        [SerializeField, Tooltip("log Folder name to use (will be created in \\Users\\%USERNAME%\\AppData\\LocalLow\\COMPANYNAME\\APPNAME\\)")]
        private string logFolderName = "VNT_Logs";

        [SerializeField, Tooltip("gameobjects which have loggable components on them that should be handled by this logger")]
        private List<GameObject> loggingTargets = new();

        [SerializeField, Tooltip("how many lines to cache before writing to file")]
        private int maxLines = 1000;

        private StringBuilder stringbuilder = new();
        private List<ILoggable> targets = null;

        [SerializeField, Tooltip("strings which trigger the logger to save the message which starts with one of them (triggers will be removed before writing)")]
        private List<string> triggerStrings = new();

        #endregion Fields

        #region Properties

        public ICustomLogger Logger { get; set; }

        #endregion Properties

        #region Public Methods

        public void Log(string logString, LogType type, object sender, string tag = "", string stackTrace = "")
        {
            // do nothing if not activated
            if (!isActive)
            {
                return;
            }

            // do nothing if not initialised
            if (!hasInitilaised)
            {
                return;
            }

            // do nothing if log is not from target Loggable
            if (!targets.Contains((ILoggable)sender))
            {
                return;
            }

            // do nothing if message does not start with trigger word
            if (!StartsWithTrigger(logString))
            {
                return;
            }

            // clean logstring
            logString = RemoveTriggerSubstrings(logString);

            // collect line
            stringbuilder.AppendLine(logString);

            // if enough collected: write to file
            if (stringbuilder.Length >= maxLines)
            {
                DumpLogToFile();
            }
        }

        /// <summary>
        /// This toggles the state of the logger.
        /// </summary>
        /// <param name="state"> </param>
        public void SetActive(bool state)
        {
            isActive = state;
        }

        #endregion Public Methods

        #region Private Methods

        private void DumpLogToFile()
        {
            Logger.Log($"adding {stringbuilder.Length} lines to {currentLogPath}", LogType.Log, this, VNTLogFormatter.VNT_Tag);

            // create file with header if it does not exist
            if (!File.Exists(currentLogPath))
            {
                File.AppendAllLines(currentLogPath, new[] { header });
            }

            // add current lines to file
            File.AppendAllText(currentLogPath, stringbuilder.ToString());

            // clear out cached lines
            stringbuilder.Clear();
        }

        private void OnDisable()
        {
            DumpLogToFile();
        }

        /// <summary>
        /// This checks the provided target objects and their children for instances of the
        /// ILoggable interface
        /// </summary>
        private void ParseLoggingTargets()
        {
            foreach (var target in loggingTargets)
            {
                targets = FindInterfaces.FindInterfacesInChildren<ILoggable>(target);
                Logger.Log($"found {targets.Count} loggables on provided logging target objects.", LogType.Log, this, VNTLogFormatter.VNT_Tag);
            }
            hasInitilaised = true;
        }

        private string RemoveTriggerSubstrings(string logString)
        {
            foreach (var triggerString in triggerStrings)
            {
                logString = logString.Replace(triggerString, "");
            }
            return logString;
        }

        private void Start()
        {
            if (!hasInitilaised)
            {
                ParseLoggingTargets();
            }

            // make sure logdir exists
            logDir = Path.Combine(Application.persistentDataPath, logFolderName);
            if (!Directory.Exists(logDir))
            {
                Directory.CreateDirectory(logDir);
                Logger.Log($"newly created logging dir at: {logDir}", LogType.Warning, this, VNTLogFormatter.VNT_Tag);
            }

            // init log path
            currentLogPath = Path.Combine(logDir, logFileName + DateTime.Now.ToString("_yyyyMMddHHmmssfff") + ".csv");
        }

        private bool StartsWithTrigger(string logString)
        {
            foreach (var triggerString in triggerStrings)
            {
                if (logString.StartsWith(triggerString))
                {
                    return true;
                }
            }
            return false;
        }

        #endregion Private Methods
    }
}