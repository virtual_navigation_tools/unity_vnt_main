﻿using System;
using System.Threading;

namespace unibi.vnt.util
{
    //based on the accepted answer here: https://stackoverflow.com/questions/273313/randomize-a-listt/1262619#1262619
    public static class ThreadSafeRandom
    {
        #region Fields

        [ThreadStatic] private static Random Local;

        #endregion Fields

        #region Properties

        public static Random ThreadInternalRandom
        {
            get { return Local ?? (Local = new Random(unchecked(Environment.TickCount * 31 + Thread.CurrentThread.ManagedThreadId))); }
        }

        #endregion Properties
    }
}