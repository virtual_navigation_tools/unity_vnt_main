using UnityEngine;

namespace unibi.vnt.util
{
    public static class FastDistanceComparison
    {
        #region Public Methods

        /// <summary>
        /// Returns true if two given points are within a given radius of each other.
        /// </summary>
        /// <param name="posA"> </param>
        /// <param name="posB"> </param>
        /// <param name="radius"> </param>
        /// <returns> </returns>
        public static bool IsWithinRadius(Vector3 posA, Vector3 posB, float radius)
        {
            float sqrCheckDist = radius * radius;
            return (SquareDistance(posA, posB) <= sqrCheckDist);
        }

        /// <summary>
        /// Returns the squared distance between two points.
        /// </summary>
        /// <param name="posA"> </param>
        /// <param name="posB"> </param>
        /// <returns> </returns>
        public static float SquareDistance(Vector3 posA, Vector3 posB)
        {
            Vector3 offset = posB - posA;
            return offset.sqrMagnitude;
        }

        #endregion Public Methods
    }
}