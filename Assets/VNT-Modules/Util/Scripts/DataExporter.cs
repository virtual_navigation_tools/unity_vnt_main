﻿using System.IO;
using System.IO.Compression;
using UnityEngine;

namespace unibi.vnt.util
{
    public static class DataExporter
    {
        #region Public Methods

        /// <summary>
        /// Creates a zip archive of given directory at given location.
        /// </summary>
        /// <param name="sourceDir"> the folder to zip </param>
        /// <param name="targetDir"> where to put the zip </param>
        public static void ExportFolderAsArchive(DirectoryInfo sourceDir, DirectoryInfo targetDir)
        {
            var targetName = targetDir.FullName + ".zip";
            if (File.Exists(targetName))
            {
                File.Delete(targetName);
                Debug.LogFormat("output file already present at '{0}'. deleting old version!", targetName);
            }
            ZipFile.CreateFromDirectory(sourceDir.FullName, targetName);
        }

        #endregion Public Methods

        #region Private Methods

        /// <summary>
        /// Creates a copy of a directory at given new directory.
        /// </summary>
        /// <param name="sourceDir"> the directory to copy </param>
        /// <param name="targetDir"> where to put the copy </param>
        private static void ExportFolder(DirectoryInfo sourceDir, DirectoryInfo targetDir)
        {
            Directory.CreateDirectory(targetDir.FullName);

            // Copy each file into the new directory.
            foreach (FileInfo fi in sourceDir.GetFiles())
            {
                fi.CopyTo(Path.Combine(targetDir.FullName, fi.Name), true);
            }

            // Copy each subdirectory using recursion.
            foreach (DirectoryInfo diSourceSubDir in sourceDir.GetDirectories())
            {
                DirectoryInfo nextTargetSubDir =
                    targetDir.CreateSubdirectory(diSourceSubDir.Name);
                ExportFolder(diSourceSubDir, nextTargetSubDir);
            }
        }

        #endregion Private Methods
    }
}