﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace unibi.vnt.util
{
    /// <summary>
    /// from: https://answers.unity.com/questions/863509/how-can-i-find-all-objects-that-have-a-script-that.html
    /// </summary>
    public static class FindInterfaces
    {
        #region Public Methods

        /// <summary>
        /// Traverses the scene graph from the root object and returns all instances of given interface.
        /// </summary>
        /// <typeparam name="T"> the interface type to look for </typeparam>
        /// <returns> </returns>
        public static List<T> FindAllInterfacesInScene<T>()
        {
            List<T> interfaces = new List<T>();
            GameObject[] rootGameObjects = SceneManager.GetActiveScene().GetRootGameObjects();
            foreach (var rootGameObject in rootGameObjects)
            {
                interfaces.AddRange(FindInterfacesInChildren<T>(rootGameObject));
            }
            return interfaces;
        }

        /// <summary>
        /// Traverses the object tree starting with the given root object and returns all instances
        /// of given interface.
        /// </summary>
        /// <typeparam name="T"> the interface type to look for </typeparam>
        /// <param name="rootGameObject"> the root object </param>
        /// <returns> </returns>
        public static List<T> FindInterfacesInChildren<T>(GameObject rootGameObject)
        {
            var interfaces = new List<T>();
            T[] childrenInterfaces = rootGameObject.GetComponentsInChildren<T>();
            foreach (var childInterface in childrenInterfaces)
            {
                interfaces.Add(childInterface);
            }
            return interfaces;
        }

        #endregion Public Methods
    }
}