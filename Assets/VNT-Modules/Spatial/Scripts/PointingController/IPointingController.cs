﻿using UnityEngine;

namespace unibi.vnt.spatial
{
    public interface IPointingController
    {
        #region Properties

        /// <summary>
        /// Info on possible target hit by last raycast. Is null when no valid target was hit.
        /// </summary>
        RaycastHit? HitInfo { get; }

        /// <summary>
        /// The target collecter attached to this pointing controller
        /// </summary>
        ITargetCollector TargetCollector { get; }

        /// <summary>
        /// Position of target hit by last raycast. Is null when no valid target was hit.
        /// </summary>
        Vector3? TargetCoordinates { get; }

        /// <summary>
        /// Possible target object of last raycast. Is null when no valid target was hit.
        /// </summary>
        GameObject TargetObject { get; }

        #endregion Properties

        #region Public Methods

        /// <summary>
        /// Computes the normal of an object's surface at the point it was hit by a Raycast.
        /// </summary>
        /// <param name="hit"> the raycast hit object </param>
        /// <returns> </returns>
        Quaternion ComputeHitPointSurfaceNormal(RaycastHit hit);

        #endregion Public Methods
    }
}