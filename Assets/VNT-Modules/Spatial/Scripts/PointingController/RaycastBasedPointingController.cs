﻿using unibi.vnt.util;
using UnityEngine;

namespace unibi.vnt.spatial
{
    public class RaycastBasedPointingController : MonoBehaviour, IPointingController, ILoggable
    {
        #region Fields

        [SerializeField, Tooltip("needs a GameObject holding an implementation of IRayCaster")]
        private GameObject _rayCaster = null;

        [SerializeField, Tooltip("needs a GameObject holding an implementation of ITargetCollector")]
        private GameObject _targetCollector = null;

        #endregion Fields

        #region Properties

        public RaycastHit? HitInfo => GetHitInfo();
        public ICustomLogger Logger { get; set; }
        public ITargetCollector TargetCollector { get => targetCollector; }
        public Vector3? TargetCoordinates => GetTargetCoordinates(GetHitInfo());
        public GameObject TargetObject => GetTargetObject(GetHitInfo());
        private IRayCaster rayCaster => _rayCaster.GrabImplementation<IRayCaster>();
        private ITargetCollector targetCollector => _targetCollector.GrabImplementation<ITargetCollector>();

        #endregion Properties

        #region Public Methods

        public Quaternion ComputeHitPointSurfaceNormal(RaycastHit hit)
        {
            Vector3 hitNormal = hit.normal * Mathf.Rad2Deg;
            return Quaternion.FromToRotation(Vector3.up, hitNormal); // set prefab y axis to collider hit surface normal
        }

        #endregion Public Methods

        #region Private Methods

        private RaycastHit? GetHitInfo()
        {
            RaycastHit? nullableHit = null; // init return var as null
            Ray ray = rayCaster.CastRay(); // use attached raycaster to see where player is pointing
            nullableHit = GetRaycastHit(ray); // see if we hit something...
            return nullableHit;
        }

        private RaycastHit? GetRaycastHit(Ray ray)
        {
            RaycastHit? target = null;
            RaycastHit hit = new RaycastHit();
            if (Physics.Raycast(ray, out hit))
            {
                // if there is a target, we return the raycasthit
                if (TryGetRayCastTarget(hit) != null)
                {
                    target = hit;
                }
            }
            // otherwise, the return is still null here
            return target;
        }

        private Vector3? GetTargetCoordinates(RaycastHit? nullableHit)
        {
            Vector3? targetPos = null;
            if (nullableHit != null)
            {
                // if we hit something, check if it's a valid target and assign it to return var if yes
                targetPos = nullableHit.GetValueOrDefault().point;
            }
            return targetPos;
        }

        private GameObject GetTargetObject(RaycastHit? nullableHit)
        {
            GameObject targetObject = null; // init return var as null
            if (nullableHit != null)
            {
                // if we hit something, check if it's a valid target and assign it to return var if yes
                targetObject = TryGetRayCastTarget(nullableHit.GetValueOrDefault());
            }
            return targetObject;
        }

        private GameObject TryGetRayCastTarget(RaycastHit hit)
        {
            GameObject targetObject = null;
            foreach (var possibleTarget in targetCollector.AvailableTargets)
            {
                // if a target was hit, assign it to return var and break loop
                if (possibleTarget.transform == hit.collider.transform)
                {
                    targetObject = possibleTarget;
                    break;
                }
            }
            return targetObject;
        }

        #endregion Private Methods
    }
}