﻿using System;
using unibi.vnt.util;
using UnityEngine;

namespace unibi.vnt.spatial
{
    public class DotProductBasedRayTargetFinder : MonoBehaviour, IRayTargetFinder, ILoggable
    {
        #region Fields

        [SerializeField, Tooltip("needs a GameObject holding an implementation of ITargetCollector")]
        private GameObject _targetCollector = null;

        [Tooltip("Reduce this value to make target aquisition easier, increase to make it more strict.")]
        [Range(-1f, 1f)]
        [SerializeField] private float minMatchPercentage = 0.98f;

        #endregion Fields

        #region Events

        public event EventHandler<(GameObject, RaycastHit, Ray)> OnTargetHit;

        #endregion Events

        #region Properties

        public ICustomLogger Logger { get; set; }
        private ITargetCollector targetCollector => _targetCollector.GrabImplementation<ITargetCollector>();

        #endregion Properties

        #region Public Methods

        public GameObject CheckForTargetHit(Ray ray)
        {
            GameObject obtainedTarget = null; // init target hit as null
            RaycastHit hit = new RaycastHit();

            // if there are possible targets, check if one was hit
            if (targetCollector.AvailableTargets.Count >= 0)
            {
                if (Physics.Raycast(ray, out hit))
                {
                    obtainedTarget = DoDirectionBasedTargetSelection(ray);
                }
            }
            // inform user if no possible targets were provided
            else
            {
                Logger.Log("Could not locate ray target, list of possible targets is empty.", LogType.Warning, this);
            }

            if (obtainedTarget != null)
            {
                OnTargetHit?.Invoke(this, (obtainedTarget, hit, ray));
            }
            // finally, return obtained target (might still be null!)
            return obtainedTarget;
        }

        #endregion Public Methods

        #region Private Methods

        private GameObject DoDirectionBasedTargetSelection(Ray ray)
        {
            GameObject currentBestMatch = null;
            float currentMatchPercentage = 0;
            /*
             * For each possible target, we determine how well the raycast direction
             * and the direction between actor (raycast origin) and possible target align.
             */
            foreach (var currentPossibleTarget in targetCollector.AvailableTargets)
            {
                /*
                 * To determine the match between ray cast direction and actor-object direction,
                 * we first determine the direction from ray origin to possible target.
                 */
                Vector3 objectDirection = currentPossibleTarget.transform.position - ray.origin;
                /*
                 * Then, we determine the match between ray direction and object direction.
                 * From the Unity documentation:
                 * "The dot product is a float value equal to the magnitudes of the two vectors multiplied together
                 * and then multiplied by the cosine of the angle between them.
                 * For normalized vectors Dot returns 1 if they point in exactly the same direction,
                 * -1 if they point in completely opposite directions and zero if the vectors are perpendicular."
                 */
                float matchPercentage = Vector3.Dot(ray.direction.normalized, objectDirection.normalized);
                /*
                 * If the obtained match percentage is higher then the minimum required match
                 * and better then the best found match so far, we set the current possible target as the best match.
                 */
                if (matchPercentage > minMatchPercentage && matchPercentage > currentMatchPercentage)
                {
                    currentMatchPercentage = matchPercentage;
                    currentBestMatch = currentPossibleTarget;
                }
            }
            Logger.Log($"Best match was {currentBestMatch} at {currentMatchPercentage}% match.", LogType.Log, this);
            /*
             * Finally, we return the best match (can be null).
             */
            return currentBestMatch;
        }

        #endregion Private Methods
    }
}