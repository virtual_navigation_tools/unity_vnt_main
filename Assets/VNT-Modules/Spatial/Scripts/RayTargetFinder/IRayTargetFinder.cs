﻿using System;
using UnityEngine;

namespace unibi.vnt.spatial
{
    public interface IRayTargetFinder
    {
        #region Events

        /// <summary>
        /// Event being fired when target was hit.
        /// </summary>
        event EventHandler<(GameObject, RaycastHit, Ray)> OnTargetHit;

        #endregion Events

        #region Public Methods

        /// <summary>
        /// Performs a check to see if given ray hit a target.
        /// </summary>
        /// <param name="ray"> the ray object </param>
        /// <returns> </returns>
        GameObject CheckForTargetHit(Ray ray);

        #endregion Public Methods
    }
}