﻿using System;
using System.Collections.Generic;
using unibi.vnt.util;
using UnityEngine;

namespace unibi.vnt.spatial
{
    public class ColliderBasedRayTargetFinder : MonoBehaviour, IRayTargetFinder, ILoggable
    {
        #region Fields

        [SerializeField, Tooltip("needs a GameObject holding an implementation of ITargetCollector")]
        private GameObject _targetCollector = null;

        #endregion Fields

        #region Events

        public event EventHandler<(GameObject, RaycastHit, Ray)> OnTargetHit;

        #endregion Events

        #region Properties

        public ICustomLogger Logger { get; set; }
        public HashSet<GameObject> PossibleTargets { get; private set; }
        private ITargetCollector targetCollector => _targetCollector.GrabImplementation<ITargetCollector>();

        #endregion Properties

        #region Public Methods

        /// <summary>
        /// Add possible targets to track
        /// </summary>
        /// <param name="newTargets"> the list of targets </param>
        public void AddTargets(List<GameObject> newTargets)
        {
            PossibleTargets.UnionWith(newTargets);
        }

        public GameObject CheckForTargetHit(Ray ray)
        {
            GameObject target = null; // init target hit as null
            RaycastHit hit = new RaycastHit();

            // if there are possible targets, check if one was hit
            if (targetCollector.AvailableTargets.Count >= 0)
            {
                if (Physics.Raycast(ray, out hit))
                {
                    foreach (var possibleTarget in targetCollector.AvailableTargets)
                    {
                        // if a target was hit, assign it to return var and break loop
                        if (possibleTarget.transform == hit.collider.transform)
                        {
                            Debug.Log("hit");
                            target = possibleTarget;
                            break;
                        }
                    }
                }
            }

            if (target != null)
            {
                OnTargetHit?.Invoke(this, (target, hit, ray));
            }
            return target;
        }

        /// <summary>
        /// Remove targets from tracking
        /// </summary>
        /// <param name="targetstoRemove"> the targets to remove </param>
        public void RemoveTargets(List<GameObject> targetstoRemove)
        {
            foreach (GameObject item in targetstoRemove)
            {
                PossibleTargets.Remove(item);
            }
        }

        #endregion Public Methods

        #region Private Methods

        private void Awake()
        {
            PossibleTargets = new HashSet<GameObject>();
        }

        #endregion Private Methods
    }
}