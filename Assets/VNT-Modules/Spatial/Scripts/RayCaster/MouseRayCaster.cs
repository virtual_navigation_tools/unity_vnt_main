﻿using unibi.vnt.util;
using UnityEngine;

namespace unibi.vnt.spatial
{
    public class MouseRayCaster : MonoBehaviour, IRayCaster, ILoggable
    {
        #region Fields

        [SerializeField]
        private Camera sourceCamera = null;

        #endregion Fields

        #region Properties

        public ICustomLogger Logger { get; set; }
        public GameObject SourceObject => sourceCamera.gameObject;

        #endregion Properties

        #region Public Methods

        public Ray CastRay()
        {
            if (sourceCamera == null)
            {
                throw new MissingComponentException("No Source Camera set for ray casting!" +
                    $" Make sure to assign a camera in the inspector or call '{nameof(SetSourceCamera)}'" +
                    $" to assign a camera before calling '{nameof(CastRay)}'!");
            }
            return sourceCamera.ScreenPointToRay(Input.mousePosition);
        }

        public void SetSourceCamera(Camera sourceCam)
        {
            sourceCamera = sourceCam;
        }

        #endregion Public Methods

        #region Private Methods

        private void Start()
        {
            if (sourceCamera == null)
            {
                Logger.Log($"No Source Camera set for MouseRayCaster on {gameObject.name}.", LogType.Warning, this);
            }
        }

        #endregion Private Methods
    }
}