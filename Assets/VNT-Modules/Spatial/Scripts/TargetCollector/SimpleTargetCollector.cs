﻿using System.Collections.Generic;
using unibi.vnt.util;
using UnityEngine;

namespace unibi.vnt.spatial
{
    public class SimpleTargetCollector : MonoBehaviour, ITargetCollector, ILoggable
    {
        #region Fields

        private HashSet<GameObject> availableTargets = new();

        [Tooltip("Add objects here to make them targetable.")]
        [SerializeField] private List<GameObject> manuallyAssignedTargets = new();

        #endregion Fields

        #region Properties

        public HashSet<GameObject> AvailableTargets { get => availableTargets; }

        public ICustomLogger Logger { get; set; }

        #endregion Properties

        #region Public Methods

        public void AddTargets(List<GameObject> targets)
        {
            availableTargets.UnionWith(targets);
        }

        public void AddTargets(GameObject target)
        {
            availableTargets.Add(target);
        }

        public void ClearTargets()
        {
            availableTargets.Clear();
            Logger.Log($"cleared all targets from list.", LogType.Log, this);
        }

        public void RemoveTargets(List<GameObject> targets)
        {
            foreach (var target in targets)
            {
                availableTargets.Remove(target);
                Logger.Log($"removed {target} from list of targetable objects.", LogType.Log, this);
            }
        }

        public void RemoveTargets(GameObject target)
        {
            availableTargets.Remove(target);
        }

        #endregion Public Methods

        #region Private Methods

        private void Awake()
        {
            if (manuallyAssignedTargets.Count > 0)
            {
                AddTargets(manuallyAssignedTargets);
            }
        }

        #endregion Private Methods
    }
}