﻿using System.Collections.Generic;
using unibi.vnt.util;
using UnityEngine;

namespace unibi.vnt.spatial
{
    public class HighlightTargetVisualiser : MonoBehaviour, ITargetingVisualiser, ILoggable
    {
        #region Fields

        [SerializeField]
        private Material highlightMaterial = null;

        private Dictionary<GameObject, Material> knownTargets = new Dictionary<GameObject, Material>();

        #endregion Fields

        #region Properties

        public ICustomLogger Logger { get; set; }

        #endregion Properties

        #region Public Methods

        public void SetTargeted(GameObject target)
        {
            Renderer targetRenderer = target.transform.GetComponent<Renderer>();
            if (targetRenderer != null)
            {
                // save original material if we haven't already
                if (!knownTargets.ContainsKey(target))
                {
                    knownTargets.Add(target, targetRenderer.material);
                }
                // assign highlight material
                targetRenderer.material = highlightMaterial;
            }
            else
            {
                Logger.Log($"Tried to highlight target {target.name} without attached renderer.", LogType.Error, this);
            }
        }

        public void SetUntargeted(GameObject target)
        {
            Renderer targetRenderer = target.transform.GetComponent<Renderer>();
            if (targetRenderer != null)
            {
                if (knownTargets.ContainsKey(target))
                {
                    // restore original material
                    targetRenderer.material = knownTargets[target];
                }
                else
                {
                    Logger.Log($"Tried to un-target previously unknown object: {target}. Could not restore original material.", LogType.Error, this);
                }
            }
            else
            {
                Logger.Log($"Tried to highlight target {target.name} without attached renderer.", LogType.Error, this);
            }
        }

        #endregion Public Methods
    }
}