﻿using System.Collections.Generic;
using unibi.vnt.guidance;
using unibi.vnt.util;
using UnityEngine;

public class ExampleGuidanceUsage : MonoBehaviour, ILoggable
{
    #region Fields

    /// <summary>
    /// this is an arrow object, that can be set to point at the next waypoint.
    /// </summary>
    [SerializeField]
    private Arrow arrowPrefab;

    /// <summary>
    /// we make use of the IGuidanceController interface and can decide somewhere else, which
    /// implementation to use.
    /// </summary>
    private IGuidanceController controller;

    /// <summary>
    /// this is the GameObject to be tracked by the guidance system.
    /// </summary>
    [SerializeField]
    private GameObject playerObject = null;

    /// <summary>
    /// these are the positions at which we want to create waypoints.
    /// </summary>
    [SerializeField]
    private List<Vector3> positions = new List<Vector3>();

    /// <summary>
    /// this determines the trigger radius for all waypoints.
    /// </summary>
    [SerializeField, Range((float)0.01d, 10)]
    private double triggerRadius = 2;

    /// <summary>
    /// this is a prefab for new waypoints.
    /// </summary>
    [SerializeField]
    private GameObject waypointPrefab = null;

    /// <summary>
    /// we make use of the IWaypoint interface and can decide somewhere else, which implementation
    /// to use.
    /// </summary>
    private List<IWaypoint> waypoints = new List<IWaypoint>();

    #endregion Fields

    #region Properties

    /// <summary>
    /// we can use an external logger to toggle logging and format the output.
    /// </summary>
    public ICustomLogger Logger { get; set; }

    #endregion Properties

    #region Public Methods

    /// <summary>
    /// Here we create some waypoints based on the coordinates set in the inspector. This function
    /// also determines the eventual type of waypoint to be used as WaypointWithtTiggerRadius. In a
    /// more complex experiment the waypoints might be created by some other function reading out a
    /// config file and then delivering them to the guidance system.
    /// </summary>
    public void CreateWaypoints()
    {
        if (positions.Count > 0)
        {
            int num = 0;
            foreach (var pos in positions)
            {
                num += 1;
                GameObject newObj = Instantiate(waypointPrefab, pos, Quaternion.identity);
                WaypointWithTriggerRadius waypoint = newObj.GetComponent<WaypointWithTriggerRadius>();
                if (!waypoint)
                {
                    waypoint = newObj.AddComponent<WaypointWithTriggerRadius>();
                }
                waypoint.Initialise(playerObject, "WP_" + num, triggerRadius);
                waypoints.Add(waypoint);
            }
        }
        else
        {
            var log = $"No waypoints set in inspector. Please assign some in the {positions} field";
            Logger.Log(log, LogType.Warning, this, VNTLogFormatter.Example_Tag);
        }
    }

    /// <summary>
    /// In this example, we initialise the controller via a button calling this method from the
    /// inspector. In a more complex experiment, the controller could be (re-)initialised frequently
    /// by other parts of the program to use a new set of waypoints.
    /// </summary>
    public void InitialiseController()
    {
        controller.Initialise(waypoints, playerObject);
    }

    public void PauseGuidance()
    {
        GuidanceState controllerState = controller.GetCurrentState();
        if (controllerState == GuidanceState.Active)
        {
            controller.PauseGuidance();
        }
        else
        {
            var log = $"Cannot pause guidance, controller is in state {controllerState}.";
            Logger.Log(log, LogType.Warning, this, VNTLogFormatter.Example_Tag);
        }
    }

    public void ResetController()
    {
        controller.Reset();
    }

    public void ResumeGuidance()
    {
        GuidanceState controllerState = controller.GetCurrentState();
        if (controllerState == GuidanceState.Paused)
        {
            controller.ResumeGuidance();
        }
        else
        {
            var log = $"Cannot resume guidance, controller is in state {controllerState}.";
            Logger.Log(log, LogType.Warning, this, VNTLogFormatter.Example_Tag);
        }
    }

    public void StartGuidance()
    {
        GuidanceState controllerState = controller.GetCurrentState();
        if (controllerState == GuidanceState.Inactive && controllerState != GuidanceState.Uninitialised)
        {
            controller.StartGuidance();
        }
        else
        {
            var log = $"Cannot start guidance, controller is in state {controllerState}.";
            Logger.Log(log, LogType.Warning, this, VNTLogFormatter.Example_Tag);
        }
    }

    public void StopGuidance()
    {
        GuidanceState controllerState = controller.GetCurrentState();
        if (controllerState == GuidanceState.Active)
        {
            controller.StopGuidance();
        }
        else
        {
            var log = $"Cannot stop guidance, controller is in state {controllerState}.";
            Logger.Log(log, LogType.Warning, this, VNTLogFormatter.Example_Tag);
        }
    }

    #endregion Public Methods

    #region Private Methods

    private void Awake()
    {
        // set target fps on game start
        Application.targetFrameRate = 60;
    }

    /// <summary>
    /// a method handling the event raised by the controller when the last waypoint is reached.
    /// </summary>
    /// <param name="sender"> the waypoint </param>
    /// <param name="triggerPosition"> the position of the player when they reached the waypoint. </param>
    private void HandleLastWaypointReached(object sender, IWaypoint wp)
    {
        controller.StopGuidance();
    }

    /// <summary>
    /// a method handling the event raised by our waypoints on being reached by the player.
    /// </summary>
    /// <param name="sender"> the waypoint </param>
    /// <param name="triggerPosition"> the position of the player when they reached the waypoint. </param>
    private void HandleWaypointReached(object sender, Vector3 triggerPosition)
    {
        controller.HandleWaypointReached(sender, triggerPosition);
    }

    /// <summary>
    /// we always make sure to unsubscribe from events at the end of the lifecycle of our class
    /// instance to prevent memory leaks.
    /// </summary>
    private void OnDestroy()
    {
        WaypointWithTriggerRadius.OnReached -= HandleWaypointReached;
        GuidanceController.OnLastWaypointReached -= HandleLastWaypointReached;
    }

    private void Start()
    {
        if (!arrowPrefab)
        {
            Logger.Log("No Arrow prefab found, please assign one in the inspector!", LogType.Error, this, VNTLogFormatter.Example_Tag);
        }
        if (!waypointPrefab)
        {
            Logger.Log("No Waypoint prefab found, please assign one in the inspector!", LogType.Error, this, VNTLogFormatter.Example_Tag);
        }
        if (!playerObject)
        {
            Logger.Log("No player object found, please assign one in the inspector!", LogType.Error, this, VNTLogFormatter.Example_Tag);
        }
        var arrow = Instantiate(arrowPrefab);

        // here we decide what implementation of IGuidanceController to use, this could also be
        // decided outside of this class and then injected.
        controller = new SimpleGuidanceController(arrow);

        // here we subscribe to an event raised by our chosen waypoint implementation
        WaypointWithTriggerRadius.OnReached += HandleWaypointReached;

        /// here we subscribe to an event fired when the last waypoint is reached. Note that the
        /// events are fired by the abstract base class we have written to implement
        /// IGuidanceController. This way, the observers can listen to these events without having
        /// to know, what concrete derived implementation of the GuidanceController (in this case
        /// SimpleGuidanceController) is used.
        GuidanceController.OnLastWaypointReached += HandleLastWaypointReached;
    }

    #endregion Private Methods
}