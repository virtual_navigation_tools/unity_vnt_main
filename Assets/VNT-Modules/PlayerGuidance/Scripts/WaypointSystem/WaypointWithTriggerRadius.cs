﻿using System;
using unibi.vnt.util;
using UnityEngine;

namespace unibi.vnt.guidance
{
    /// <summary>
    /// A basic implementation of the IWaypoint interface. The waypoint fires an event when a
    /// tracked GameObject moves within a certain radius.
    /// </summary>
    public class WaypointWithTriggerRadius : MonoBehaviour, IWaypoint, ILoggable
    {
        #region Fields

        [SerializeField]
        private DistanceChecker check = null;

        private bool isInitialised = false;
        private bool isReachable = false;
        private bool isVisible = false;

        [SerializeField]
        private new string name = "UnnamedWaypoint";

        [SerializeField]
        private Renderer[] renderers = null;

        [SerializeField]
        [Range((float)0.01d, 10)]
        private double triggerRadius = 2;

        #endregion Fields

        #region Events

        public static event EventHandler<Vector3> OnReached;

        #endregion Events

        #region Properties

        public ICustomLogger Logger { get; set; }

        #endregion Properties

        #region Public Methods

        public void Delete()
        {
            Destroy(gameObject);
        }

        public void Disable()
        {
            if (isInitialised)
            {
                check.Disable();
                isReachable = false;
            }
            else
            {
                var log = $"Trying to disable, but {name} has not yet been initialised!";
                Logger.Log(log, LogType.Warning, this);
            }
        }

        public void Enable()
        {
            if (isInitialised)
            {
                check.Enable();
                isReachable = true;
            }
            else
            {
                var log = $"Trying to enable, but {name} has not yet been initialised!";
                Logger.Log(log, LogType.Warning, this);
            }
        }

        public GameObject GetGameObject() => gameObject;

        public string GetName() => name;

        public void Hide()
        {
            if (isInitialised)
            {
                foreach (Renderer renderer in renderers)
                {
                    renderer.enabled = false;
                }
                isVisible = false;
            }
            else
            {
                var log = $"Trying to hide waypoint, but {name} has not yet been initialised!";
                Logger.Log(log, LogType.Warning, this);
            }
        }

        public void Initialise
            (
                GameObject playerObject,
                string name,
                double triggerRadius
            )
        {
            // set required variables
            this.name = name;
            this.triggerRadius = triggerRadius;

            // get attached renderers
            renderers = gameObject.GetComponentsInChildren<Renderer>();

            // subscribe to distance checker event
            DistanceChecker.OnThresholdDistanceReached += HandleDistanceReached;

            // set random Y rotation for visuals
            gameObject.transform.rotation = Quaternion.Euler(0, UnityEngine.Random.Range(0f, 360f), 0);

            // add distance checker if necessary
            if (!TryGetComponent(out check))
            {
                check = gameObject.AddComponent<DistanceChecker>();
            }
            // initialise it
            check.Initialise(playerObject.transform,
                             gameObject.transform.position,
                             this.triggerRadius,
                             DistanceChecker.Mode.Smaller);
            // set initialised flag
            isInitialised = true;
            // deactivate self for now
            Hide();
            Disable();
        }

        public bool IsReachable() => isReachable;

        public bool IsVisible() => isVisible;

        public void Show()
        {
            if (isInitialised)
            {
                foreach (Renderer renderer in renderers)
                {
                    renderer.enabled = true;
                }
                isVisible = true;
            }
            else
            {
                var log = $"Trying to show waypoint, but {name} has not yet been initialised!";
                Logger.Log(log, LogType.Warning, this);
            }
        }

        #endregion Public Methods

        #region Private Methods

        private void HandleDistanceReached(object sender, Vector3 position)
        {
            if ((DistanceChecker)sender == check)
            {
                OnReached?.Invoke(this, position);
            }
        }

        private void OnDestroy()
        {
            DistanceChecker.OnThresholdDistanceReached -= HandleDistanceReached;
        }

        #endregion Private Methods
    }
}