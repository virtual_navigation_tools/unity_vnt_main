﻿using UnityEngine;

namespace unibi.vnt.guidance
{
    /// <summary>
    /// This interface defines the contract to be fulfilled by all implementations of the waypoint
    /// concept. Other classes (like the guidance controller) will reference the interface rather
    /// than specific implementations wherever possible, to maximise freedom in user design.
    /// </summary>
    public interface IWaypoint
    {
        #region Public Methods

        /// <summary>
        /// Delete the waypoint.
        /// </summary>
        void Delete();

        /// <summary>
        /// Make the waypoint unreachable by the player.
        /// </summary>
        void Disable();

        /// <summary>
        /// Make the waypoint reachable by the player.
        /// </summary>
        void Enable();

        /// <summary>
        /// Retrieve the GameObject the waypoint is linked to. (i.e. its visual representation)
        /// </summary>
        /// <returns> GameObject linked to the waypoint </returns>
        GameObject GetGameObject();

        /// <summary>
        /// Retrieve the name of the waypoint as a string.
        /// </summary>
        /// <returns> waypoint name </returns>
        string GetName();

        /// <summary>
        /// Hide the waypoint from the player.
        /// </summary>
        void Hide();

        /// <summary>
        /// Check if the waypoint is currently reachable.
        /// </summary>
        /// <returns> true if reachable, false otherwise </returns>
        bool IsReachable();

        /// <summary>
        /// Check if the waypoint is currently visible.
        /// </summary>
        /// <returns> true if visible, false otherwise </returns>
        bool IsVisible();

        /// <summary>
        /// Make the waypoint visible to the player.
        /// </summary>
        void Show();

        #endregion Public Methods
    }
}