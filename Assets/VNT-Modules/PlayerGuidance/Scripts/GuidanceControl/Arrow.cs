﻿using unibi.vnt.util;
using UnityEngine;

namespace unibi.vnt.guidance
{
    public class Arrow : MonoBehaviour, ILoggable
    {
        #region Fields

        [SerializeField]
        private Vector3 anchorPos = new Vector3(0, 0.5f, 1.8f);

        [SerializeField]
        private GameObject arrowObject = null;

        private Transform intermediateTransform = null;

        [SerializeField, Range(0.0001f, 20)]
        private float smoothingFactor = 1;

        [SerializeField]
        private GameObject target = null;

        #endregion Fields

        #region Properties

        public ICustomLogger Logger { get; set; }

        #endregion Properties

        #region Public Methods

        /// <summary>
        /// Sets the arrow object as a child of the provided player object.
        /// </summary>
        /// <param name="player"> the player object which will be the parent of the arrow </param>
        public void AnchorToPlayer(GameObject player)
        {
            gameObject.transform.SetParent(player.transform);
            gameObject.transform.localPosition = anchorPos;
        }

        /// <summary>
        /// Hide the arrow object
        /// </summary>
        public void DisableAndHide()
        {
            target = null;
            arrowObject.SetActive(false);
        }

        /// <summary>
        /// Make the arrow point at the current target.
        /// </summary>
        public void PointAtTarget()
        {
            arrowObject.SetActive(true);
        }

        /// <summary>
        /// Set a new target for the arrow.
        /// </summary>
        /// <param name="target"> the target object </param>
        public void SetTarget(GameObject target)
        {
            this.target = target;
            intermediateTransform = target.transform;
        }

        #endregion Public Methods

        #region Private Methods

        private void Start()
        {
            if (!arrowObject)
            {
                arrowObject = gameObject;
                Logger.Log("no arrow object assigned, assuming parent object ist arrow.", LogType.Warning, this);
            }
        }

        private void TrackTarget(Transform actualTarget)
        {
            intermediateTransform.localPosition = Vector3.Lerp(intermediateTransform.localPosition, actualTarget.localPosition, smoothingFactor * Time.deltaTime);
            intermediateTransform.localRotation = Quaternion.Lerp(intermediateTransform.localRotation, actualTarget.localRotation, smoothingFactor * Time.deltaTime);
            gameObject.transform.LookAt(intermediateTransform);
        }

        private void Update()
        {
            if (target == null)
            {
                return;
            }
            TrackTarget(target.transform);
        }

        #endregion Private Methods
    }
}