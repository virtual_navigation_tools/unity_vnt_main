﻿using System.Collections.Generic;
using UnityEngine;

namespace unibi.vnt.guidance
{
    public enum GuidanceState
    {
        Active, Paused, Inactive, Uninitialised
    }

    /// <summary>
    /// This interface defines the contract to be fulfilled by all implementations of the guidance
    /// controller concept. Other classes (like managers making use of the controller) will
    /// reference the interface rather than specific implementations wherever possible, to maximise
    /// freedom in user design.
    /// </summary>
    public interface IGuidanceController
    {
        #region Public Methods

        /// <summary>
        /// This can be called to retrieve the current state of the guidance controller.
        /// </summary>
        /// <returns> current state of the controller </returns>
        GuidanceState GetCurrentState();

        /// <summary>
        /// This can be used as a delegate or called by some other class registering a reached waypoint.
        /// </summary>
        /// <param name="sender"> the object broadcasting the event </param>
        /// <param name="triggerPosition"> the world coordinates at which the event was triggered </param>
        void HandleWaypointReached(object sender, Vector3 triggerPosition);

        /// <summary>
        /// Before we can use a guidance controller, it needs to know which waypoints to use and
        /// which GameObject (usually representing the player) to track.
        /// </summary>
        /// <param name="waypoints"> the waypoints to use </param>
        /// <param name="trackedObject"> the object to track (usually the player) </param>
        void Initialise(List<IWaypoint> waypoints, GameObject trackedObject);

        /// <summary>
        /// This should be called to temporariliy suspend guidance.
        /// </summary>
        void PauseGuidance();

        /// <summary>
        /// This should be called to reset the controller to an uninitialised state.
        /// </summary>
        void Reset();

        /// <summary>
        /// This should be called to resume guidance after it was suspended.
        /// </summary>
        void ResumeGuidance();

        /// <summary>
        /// This should be called to start guiding the player along the waypoints defined during initialisation.
        /// </summary>
        void StartGuidance();

        /// <summary>
        /// This should be called to terminate guiding the player.
        /// </summary>
        void StopGuidance();

        #endregion Public Methods
    }
}