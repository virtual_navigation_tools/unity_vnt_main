﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using unibi.vnt.util;
using UnityEngine;

namespace unibi.vnt.trialflow
{
    /// <summary>
    /// If the tasks to be performed within a state are simple, it can all be handled directly in
    /// the state class. This is demonstrated here. However, in the context of navigation tasks, a
    /// state might cover multiple, more complex operations, like waiting for user input, etc.. For
    /// this reason, we also provide a template for states with external state handling. For this,
    /// see the 'ExampleStateWithExternalHandler' class.
    /// </summary>
    internal class ExampleStateWithInternalHandling : TrialState, ILoggable
    {
        #region Fields

        // set a wait time
        private bool stateActive = false;

        [SerializeField]
        private int waitTimeinSeconds = 3;

        #endregion Fields

        #region Public Constructors

        public ExampleStateWithInternalHandling(string name, ICustomLogger logger) : base(name)
        {
            Logger = logger;
        }

        #endregion Public Constructors

        #region Properties

        public ICustomLogger Logger { get; set; }
        public TrialStateMachine StateMachine { get; private set; }

        #endregion Properties

        #region Public Methods

        public override void Enter(TrialStateMachine stateMachine)
        {
            stateActive = true;
            StateMachine = stateMachine;
            var log = $"We have entered the state {Name}";
            Logger.Log(log, LogType.Log, this, VNTLogFormatter.Example_Tag);
            log = "In this example state," +
                  " we only wait for a given time " +
                  "and then enter the next state," +
                  " which the choice of the next state being hardcoded in this example.";
            Logger.Log(log, LogType.Log, this, VNTLogFormatter.Example_Tag);
            WaitAndExit();
        }

        public override void Exit()
        {
            stateActive = false;
            var log = $"We are leaving the state {Name}";
            Logger.Log(log, LogType.Log, this, VNTLogFormatter.Example_Tag);
        }

        #endregion Public Methods

        #region Private Methods

        private async void WaitAndExit()
        {
            var log = $"We will now wait for {waitTimeinSeconds} seconds " +
                      $"and then transition to {ExampleStateMachine.ExampleState2.Name}.";
            Logger.Log(log, LogType.Log, this, VNTLogFormatter.Example_Tag);

            #region do the waiting and time actual wait duration

            Stopwatch stopWatch = new();
            stopWatch.Start();
            await Task.Delay(waitTimeinSeconds * 1000);
            stopWatch.Stop();
            TimeSpan ts = stopWatch.Elapsed;

            #endregion do the waiting and time actual wait duration

            log = $"Waited for {ts.TotalSeconds} out of expected {waitTimeinSeconds} seconds.";
            Logger.Log(log, LogType.Log, this, VNTLogFormatter.Example_Tag);
            stopWatch.Reset();
            if (stateActive)
            {
                log = "Initiating state transition...";
                Logger.Log(log, LogType.Log, this, VNTLogFormatter.Example_Tag);
                StateMachine.SetState(ExampleStateMachine.ExampleState2); // state transition happens here
            }
            else
            {
                log = "Timer elapsed but state machine already left state, so nothing happens.";
                Logger.Log(log, LogType.Log, this, VNTLogFormatter.Example_Tag);
            }
        }

        #endregion Private Methods
    }
}