﻿using unibi.vnt.util;
using UnityEngine;

namespace unibi.vnt.trialflow
{
    /// <summary>
    /// This class showcases the setting up and running of a TrialStateMachine.
    /// </summary>
    internal class TrialFlowExampleRunner : MonoBehaviour, ILoggable
    {
        #region Fields

        [SerializeField]
        private bool runStateMachine = false;

        private ExampleStateMachine stateMachine = null;

        #endregion Fields

        #region Properties

        public ICustomLogger Logger { get; set; }
        public bool RunStateMachine { get => runStateMachine; set => runStateMachine = value; }

        #endregion Properties

        #region Public Methods

        public void HandleLeftButton()
        {
            stateMachine.SetState(ExampleStateMachine.ExampleState1);
            var log = "Forcing Machine back to State 1";
            Logger.Log(log, LogType.Log, this, VNTLogFormatter.Example_Tag);
        }

        public void StartMachine()
        {
            if (stateMachine.CurrentState == TrialStateMachine.Empty)
            {
                stateMachine.SetState(ExampleStateMachine.ExampleState1);
                var log = "Running State Machine...";
                Logger.Log(log, LogType.Log, this, VNTLogFormatter.Example_Tag);
            }
        }

        public void StopMachine()
        {
            if (stateMachine.CurrentState != TrialStateMachine.Empty)
            {
                var log = "Resetting state machine to empty!";
                Logger.Log(log, LogType.Log, this, VNTLogFormatter.Example_Tag);
                stateMachine.SetState(TrialStateMachine.Empty);
                runStateMachine = false;
            }
        }

        #endregion Public Methods

        #region Private Methods

        private void Awake()
        {
            Application.targetFrameRate = 60;
        }

        private void Start()
        {
            stateMachine = new ExampleStateMachine(Logger);
        }

        private void Update()
        {
            if (runStateMachine == true)
            {
                StartMachine();
            }
            else
            {
                StopMachine();
            }
        }

        #endregion Private Methods
    }
}