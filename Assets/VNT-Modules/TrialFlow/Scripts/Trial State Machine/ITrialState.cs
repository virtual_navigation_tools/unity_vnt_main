﻿namespace unibi.vnt.trialflow
{
    /// <summary>
    /// Thsi interface provides the basic contract to be fulfilled by all trial states.
    /// </summary>
    public interface ITrialState
    {
        #region Properties

        /// <summary>
        /// The name of the state.
        /// </summary>
        string Name { get; }

        #endregion Properties

        #region Public Methods

        /// <summary>
        /// This method is be called to enter the state.
        /// </summary>
        /// <param name="stateMachine"> the state machine to which the state belongs </param>
        void Enter(TrialStateMachine stateMachine);

        /// <summary>
        /// This method is called to exit the state.
        /// </summary>
        void Exit();

        #endregion Public Methods
    }
}