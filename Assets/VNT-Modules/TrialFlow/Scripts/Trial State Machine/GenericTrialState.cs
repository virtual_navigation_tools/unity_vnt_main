﻿namespace unibi.vnt.trialflow
{
    public class GenericTrialState : TrialState
    {
        #region Public Constructors

        /// <summary>
        /// If we want, we can have a simple all-purpose state which simply fires the OnEnter and
        /// OnExit events defined in the abstract base class. The only unique thing about this state
        /// will be the name of its instance. Everything that should happen when an instance of this
        /// state is entered and exited will be defined in a handler class.
        /// </summary>
        /// <param name="name"> the name of the state instance </param>
        public GenericTrialState(string name) : base(name) { }

        #endregion Public Constructors
    }
}