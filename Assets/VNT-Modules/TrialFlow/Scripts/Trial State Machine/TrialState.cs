﻿using System;

namespace unibi.vnt.trialflow
{
    /// <summary>
    /// This class provides the basic template for all trial states. New states should derive from
    /// this base class.
    /// </summary>
    public abstract class TrialState : ITrialState
    {
        #region Public Constructors

        /// <summary>
        /// The constructor needs to be passed a name for the new state instance to be created. This
        /// allows for easy distinction of several instances of the same state, if necessary.
        /// </summary>
        /// <param name="name"> the name of the state instance. it should be unique! </param>
        public TrialState(string name)
        {
            Name = name;
        }

        #endregion Public Constructors

        #region Events

        /// <summary>
        /// This event is fired when the Enter() method of the state is called.
        /// </summary>
        public static event EventHandler<TrialStateMachine> OnEnter;

        /// <summary>
        /// This event is fired when the Exit() method of the state is called.
        /// </summary>
        public static event EventHandler OnExit;

        #endregion Events

        #region Properties

        /// <summary>
        /// The name of the state instance.
        /// </summary>
        public string Name { get; private set; }

        #endregion Properties

        #region Public Methods

        public virtual void Enter(TrialStateMachine stateMachine)
        {
            OnEnter?.Invoke(this, stateMachine);
        }

        public virtual void Exit()
        {
            OnExit?.Invoke(this, EventArgs.Empty);
        }

        #endregion Public Methods
    }
}