<img src="https://gitlab.ub.uni-bielefeld.de/virtual_navigation_tools/unity_vnt_main/-/raw/main/vnt_logo_hex.svg" align="right" width="600px"/>

# The Virtual Navigation Toolbox
This is the development repository for the unity virtual navigation toolbox (VNT) developed at Bielefeld University.
The repository provides tools to implement and run virtual navigation experiments using the Unity3D game engine.
To see an example of a project using the VNT, visit our showcase repository at [https://gitlab.ub.uni-bielefeld.de/virtual_navigation_tools/unity_vnt_showcase_triangle_completion](https://gitlab.ub.uni-bielefeld.de/virtual_navigation_tools/unity_vnt_showcase_triangle_completion).

You can also read more about the toolbox in our paper recently published in PLOS ONE at https://doi.org/10.1371/journal.pone.0293536.


# Project Wiki
The features of the toolbox are explained in more detail in the attached wiki.
[Click here to get directly to the wiki and the provided module examples.](https://gitlab.ub.uni-bielefeld.de/virtual_navigation_tools/unity_vnt_main/-/wikis/home)



# Contribution
If you wish to contribute to the VNT, please create a merge request and explain your suggested changes and additions.
Please follow the principles of modular design outlined in the [wiki](https://gitlab.ub.uni-bielefeld.de/virtual_navigation_tools/unity_vnt_main/-/wikis/home), when working on your contributions.
Also, please make sure to format your code using the provided VNT [CodeMaid](https://marketplace.visualstudio.com/items?itemName=SteveCadwallader.CodeMaid) [config file](https://gitlab.ub.uni-bielefeld.de/virtual_navigation_tools/unity_vnt_main/-/blob/main/CodeMaid_VNT.config) for visual studio.

For any questions please contact martin.mueller@uni-bielefeld.de.
<br clear="right"/>
# License
The project is published under under the GNU GENERAL PUBLIC LICENSE, Version 3 (see also [LICENSE](https://gitlab.ub.uni-bielefeld.de/virtual_navigation_tools/unity_vnt_main/-/blob/main/LICENSE)), meaning you are free to re-use and modify any parts of this work for your own project, at no cost.

# Integration
You can include this repository into your own repository and be able to check out any changes that might occur later down the line.
To do this, just add this repository as a subtree to your own repository. [Also see [here](https://www.geeksforgeeks.org/git-subtree/) for more information on the topic].

Use this command in your git console to add our repository as a subtree:

```
git subtree add --squash --prefix=TARGETFOLDERNAME git@gitlab.ub.uni-bielefeld.de:virtual_navigation_tools/unity_vnt_main.git main
```

Use this command to keep your local copy up to date with the main vnt repo:

```
git subtree pull --prefix=TARGETFOLDERNAME git@gitlab.ub.uni-bielefeld.de:virtual_navigation_tools/unity_vnt_main.git main --squash
```
"TARGETFOLDERNAME" refers to the subdirectoy, in which you want to place the VNT repository within your own repository.
For example, in the [showcase repository](https://gitlab.ub.uni-bielefeld.de/virtual_navigation_tools/unity_vnt_showcase_triangle_completion), this would be "Assets/MainVNTRepo".



